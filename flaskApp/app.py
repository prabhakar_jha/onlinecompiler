from flask import Flask, render_template, request
from subprocess import Popen, PIPE
import json
import os
app = Flask(__name__)


@app.route('/')
def hello():
    return render_template("home.html")

@app.route('/compile',methods = ['GET'])
def compile():
	if request.method == 'GET':
		print("In the function")
		code = request.args.get('code')
		user_input = request.args.get('user_input')
		# language_code =  request.form['language_code']
		file = open("/tmp/temp.py", 'w')
		file.write(code)
		file.close()
		os.chdir('/tmp/')
		process = Popen(['python', 'temp.py'], stdout=PIPE, stderr=PIPE)
		stdout, stderr = process.communicate()

		if stderr:
			stderr = str(stderr)
			message = stderr.replace('\n', '<br>')
		else:		
			stdout = str(stdout)	
			message = stdout.replace('\n', '<br>')
		return json.dumps({'statue': 200, 'message': str("<br />".join(message.split("\\n")))[2:-1]})


if __name__ == '__main__':
    app.run()
